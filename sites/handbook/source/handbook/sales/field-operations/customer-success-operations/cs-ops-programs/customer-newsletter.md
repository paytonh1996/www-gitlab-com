---
layout: handbook-page-toc
title: "Customer Newsletter"
Description: The customer newsletter recaps each months release and highlights important information from the field, product and/or marketing.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Overview 
This is a monthly customer newsletter that recaps each months release and highlights important information from the field, product and/or marketing

Ongoing feedback and participation from the field and product is imperative to the success of this program. If you have feedback on the current process or would like to request a certain type of content or update for future newsletters:

- Submit an issue using our [CS Programs - Idea Request](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/new) template
- Reach out in the #cs-operations Slack channel


## Target Audience 
This newsletter is sent to all customers where a `GitLab Admin` contact is identified. There are bifurcated email versions based on CSM-owned/Non-CSM owned and PubSec. For non-CSM managed customers we also send to the sold-to-contact when there is no GitLab Admin as well as Owners of SaaS instances. 

## Newsletter Tool
The newsletter is sent using Gainsight Journey Orchestrator. This tool allows Digital CS to send targeted email newsletters to customers based on the role, sales segment, region, products purchased, usage and more.
The benefit of this tool is:

- Auto-populate tokens (company name, first name, csm name, usage statistic)
- Multi-step outreaches
- Multiple email versions
- Link your emails triggered by Programs, to other features in Gainsight (e.g. CTAs - Calls to Action)


## Opportunties / Requirements 
- Create awareness
- Post Announcements and deprecations
- Share webinar invitations, customer stories, and events
- Share customer usage (roadmap request)
- Various other requests

## Format
- GitLab Release Call outs by plan and instance type
- Field callouts and important updates related to customer accounts
- Product / Marketing callouts 
- Webinars / Events / Customer Stories

## Process
- The issue for the upcoming newsletter is opened at least one week before the GitLab Release.
- Relevant stakeholders/contributors/slack channels are notified each month to provide content or review suggested content in the outline.
- Contributed content is due no later than one week before the planned send date.
Once the outline is complete, Digital CS team drafts the newsletter in a Google document linked to in the issue and then imports the content into the newsletter template in Gainsight.
- Digital CS sends the newsletter test email to relevant leaders and stakeholders for review no more than two business days before planned send date.
- Reviewers provide feedback no later than 12 pm PT on the business day before the planned send date to allow time for revisions and scheduling.
- Digital CS schedules the newsletter to send ~ 9 am PT on the planned send date.
Once the newsletter goes live, Digital CS posts a reminder in the dedicated issue
- On the Monday following the newsletter send, Digital CS captures the newsletter performance data in the issue and then closes it.

To be added to the newsletter distribution list, please reach out in the #cs-operations Slack channel.

## Measurements 
Quantitative Success Metrics

- Email open rate - Average open rate of x% in FY23.
- Click-to-open rate - Average click rate of x% in FY23.
- Usage data if applicable 

Qualitative Success Metrics

- Increased engagement from field team members/leaders and stakeholders in regards to the newsletter – feedback, requests, suggestions, etc.
- Usefulness of newsletter for customer conversations / engagement.

## Past Newsletters
All past newsletters are accessible [here](https://docs.google.com/document/d/1VkN-pqElJJtqz2vsLySnm5FDTsHVK7wyZwX_97WeyOM/edit?usp=sharing) and are logged on the customers account in SFDC and Gainsight.